//Controllers contains the functions and business logic of our Express 
//JS Application

const Task = require("../models/task");

//[CONTROLLERS]

//[SECTION] -CONTROLLERS

//[SECTION] -CREATE

//2.
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) =>{
		if(error){
			return false
		}else{
			return task
		}
	})
}
//[SECTION] -RETRIEVE
	module.exports.getAllTasks = () => {

	return Task.find({}).then(result =>{
		return result
	})
}

//[SECTION] -UPDATE
	//1.Change status of Task. pending -> 'completed'.
	//we will reference the document using it's ID field.
		module.exports.taskCompleted = (taskId) => {
			//Query/search for the desired task to update.
			//The "findyById mongoose method will look for a resources (task) which matches the ID from the URL of the request."
			//upon performing this method inside our collection, an new promise will be instantiated, so we need to be able to handle the posibble outcome of that promise.
		return Task.findById(taskId).then((found,error) =>{
			//describe how were going to handle the outcome of the promise using a selection control structure
			//error -> rejected state of the promise
			//process the document found from the collection and change the status from 'pending' => compeleted
			if (found) {
				// console.log(error); checking
				console.log(found); //document found from the db should be displayed in the terminal.
				//Modifies the status of the returned document to 'complete'
				found.status = 'Completed';
				//Save the new changes inside our database.
				//upon saving the new changes for this document a 2nd promises will be instantiated.
				//we will chain a thenable expression upon performing a save() method into our return document.
				return found.save().then((updatedTask, saveErr) => {
					//catch the state of the promise to identify a specific response.
					if (updatedTask) {
						return 'Task has been successfully modified'
					} else {
						return 'Task failed to Update';
					}
				})
			} else {
				//call out the paramater that describe the result of the query when successful.
				return 'Error!';
			};
		});
			};

			//2. Change the status of task (Completed -> Pending)
			module.exports.taskPending = (userInput) => {
				//expose this new component
				//search the database for the user Input.
				//findbyId moongose method -> will run a search query inside our database using the id field as it's reference;
				//since performing this method will have 2 posiblle states/outcome, we will chain a then expression to handle the posibble states of the promise
				//asign and invoke this new controller task to its own seperate  route.
				return Task.findById(userInput).then((result,err) => {
					//handle and catch the state of the promise.
					if (result) {
						//process the result of the query and extract the property to modify it's value.
						result.status = 'Pending';

						//save the new changes in the document
						return result.save().then((taskUpdated,error) => {
							//create a control structure to identify the proper response if the updates was successfully executed.
							if (taskUpdated) {
								return `Task ${taskUpdated.name} was updated to Pending`;
							} else {
								return 'Error when saving task updates';
							}
						})
					} else {
						return 'Something Went Wrong';

					}
				});
			};


//[SECTION] -DESTROY
	//1 Remove an existing resource inside the TASK collection.
		//expose the data across other modules other app so that it will become reusable
		//would we need an input from the user? => which resources you want to target
	module.exports.deleteTask = (idNgTask) => {
		//how will the data will be processed in order to execeute the task.
		//select which mongoose method will be used in order to acquire the desired end goal.
		//Mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas.
		//in oder to identify the location in which the function will be exectued append the model name.

		//findByIdAndRemove => this is a mongoose method which target a document using it's id field and removes the target documents from the collection.
		//upon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state in my fall on.
		//PROMISES in JS
		//=> Pending(waiting to be executed)
		//=> Fulfilled(successfully executed)
		//=> Rejected(unfulfilled promise)
		//to be able to handle the posibble states upon executing the task 
		//we are going to insert a 'thenable' expression to determin HOw we  will respond depending on the result of the promise.
		//identify the 2 possible state of the promise using a then expression
		return Task.findByIdAndRemove(idNgTask).then((fulfilled,rejected) =>{
			//identify how you will act according to the outcome.
			if (fulfilled) {
				return 'The Task has been successfully Removed';
			} else {
				return 'Failed to remove Task';
			};
			//assign a new endpoint for this route.

		});
	};









