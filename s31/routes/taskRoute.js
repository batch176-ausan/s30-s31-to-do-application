//Contains all the endpoints for our application
//We seperate the routes such that "app.js" only contains information on the server

const express = require("express");

//Create a Router instance that functions as a  middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes for our application

const router = express.Router();

const taskController = require("../controllers/taskController");

//[Routes]
//Route for getting all the tasks

router.get("/",(req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});



//Route for creating a task
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Router for Deleting a task
	//call out the routing component to register a brand new endpoint.
	//When integreating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC.
	//2 Types of End Points
	//Static Route
	//->unchanging, fixed, contants,steady
	//Dynamic Route
	//-> interchangeable or NOT FIXED,
router.delete('/:task', (req,res) => {
	//identif the task to be executed within this endpoint
	//call out the proper function for this route and indetify the source/provider of the function.
	console.log(req.params.task);
	//place the value of the path variable inside its own container.
	let taskId = req.params.task
	// res.send('Hello from delete');
	//only to check if the setup is correct

	//retrive the identify of the task by inserting the OBjectId of the resources.
	//make sure to pass down the identify of the task using the proper reference (objectID),however this time we are going to include the information as a path variable
	//Path : variable -> this will allow to URL
	//what is variable? -> container, storage of information
	//When is a path variable useful? 
	//when iserting only a single piece of information.
	//this is also useful when passing down information REST API method that does NOT include a BODY esction like 'GET'.

	//upon executing this method a promise will be initialized so we need to handle the rest of the promise in our route module.
	taskController.deleteTask(taskId).then(resultNgDelete => res.send(resultNgDelete));
});

//Router for Updating task status
	//Lets create a dynamic endpoint for this brand new route.
	router.put('/:task', (req,res) => {
		//check if your are able to acquire the values inside the path variables.
		console.log(req.params.task); //check if the value inside the paramaters of the route is properly transmitted to the server.
		let idNiTask = req.params.task; // this is declared to simplify the means of calling out the value of the path variable key.

		//identify the business logic behind this task inside the controller module.

		//call the intended controller to execute the process make sure to identify the provider/soruce of the function
		//after invoking the controller method handle the outcome
		taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
	});


//Route for Updating task status(Completed -> Pending)
	//This will be a counter procedure for the previous task.
router.put('/:task/pending', (req,res) => {
	let id = req.params.task;
	//console.log(id);//checking

	//declare the business logic aspect of this brand new task in our app.
	//invoke the task you want to execute in this route.
 taskController.taskPending(id).then(outcome => {
     res.send(outcome);
 });
});

//Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;