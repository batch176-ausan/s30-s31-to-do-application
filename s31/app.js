const express = require("express");
 const mongoose = require("mongoose");
 //Add 2nd require to connect files
 const taskRoute = require("./routes/taskRoute")

//[SECTION] Server Setup
 const app = express();

 const port = 4000;

 app.use(express.json());
 app.use(express.urlencoded({extended: true}));

 //DB SETUP

 mongoose.connect('mongodb+srv://Dmzlovesyou123:admin123@cluster0.cr80p.mongodb.net/toDo176?retryWrites=true&w=majority',{

    useNewUrlParser:true,
    useUnifiedTopology:true
});

 let db = mongoose.connection;

 db.on('error',console.error.bind(console,"Connection Error"));

 db.once('open',()=>console.log("Connected to MongoDb"));

 //Add 2nd the task route
 //localhost:4000/tasks/
 app.use("/task",taskRoute);

//PORT

 app.listen(port, () => console.log(`Server running at port ${port}`));