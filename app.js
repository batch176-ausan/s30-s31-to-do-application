//[SECTION1] Dependecies and Modules
const express = require("express");
 const mongoose = require("mongoose");

//[SECTION2] Server Setup
	//establish a connection
const app = express();
//define a path/address
const port = 4000; //Assign the port 4000 to avoid conflict in the future with
//frontend application that run on port 3000

//[SECTION3] Database Connection
//Connect to MongoDB ATLAS
//GET the credentials of your atlas user.
mongoose.connect('mongodb+srv://Dmzlovesyou123:admin123@cluster0.cr80p.mongodb.net/toDo176?retryWrites=true&w=majority',{

	//option to add to avoid deprecation warnings because of monggose/mongodb update.
	useNewUrlParser:true,
	useUnifiedTopology:true
});

//Create notifications if the connection to the db is a success or a failure.
let db = mongoose.connection;
//Let's add an on () method from our mongoose connection to show if the connection
//has succeeded or failed in both the terminal and in the browser for our client.
db.on('error',console.error.bind(console,"Connection Error"));
//once the connection is open and successful, we will output a message in the terminal
db.once('open',()=>console.log("Connected to MongoDb"));

//Middleware - A middleware, in expressjs context, are method, functions that
//acts and adds features to our application.
app.use(express.json());


//Schema
//Before we can create documents from our api to save into our database, we
//first have to determine the structure of the documents to be writeen in our
//database. This is to ensure the consistency of our documents and avoid future errors.

//Scheme acts as a blueprint for our data/document.

//Schema() constructor from monggosee to create a new schema object
const taskSchema = new mongoose.Schema({

name: String,
status: String

});

//Mongoose Model
//Models are used to connect your api to the corresponding collection in your
//database. It is a representation of your collection.

//Models uses schemas to create object that correspond to the schema. By default,
//when creating the collection from your model, the collection name is pluralized

//mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)



const Task = mongoose.model("task",taskSchema);


//Post route  to create a new task
app.post('/task',(req,res)=>{

//console.log(req.body);

//Creating a new task document by using the constructor of our Task model.
//This constructor should follow  the schema of the model.
let newTask = new Task({

name: req.body.name,
status: req.body.status

});

//.save() method from an object created by a model.
//save() method will allow us to save our document by connecting
//to our collection via our model.

//save() has 2 approaches:
//1. we can add an anynomous function to handle the
//created document or error.
//2. we can add.then() chain which will allow us to handle errors and created
//docuemnts in seperate function.


// newTask.save((error,savedTask)=>{
// 	if(error){
// 		res.send(error);
// 	}else{
// 		res.send(savedTask);
// 	}
// });
//,then() and.catch() chain
//,then() is used to handle the proper result/returned value of a function.
//If the function properly returns a value, we can a seperate function to handle it.
//.catch() is used to handle/catch the error from the use of a function. So that
//if an error occurs, we can handle the error seperately
newTask.save()
.then(result => res.send({message: "Document Creation Successful"}))
.catch(error => res.send({message: "Error in Document Creationg"}));

});

const sampleSchema = new mongoose.Schema({

 name: String,
 isActive: Boolean

});

//Get Method Request to Retrieve All Task Documents from our Collection

app.get('/task',(req,res) => {

//To query using mongoose, first access the model of the collection you want
//manipulate
//Model.find() in mongoose is similar in function to mongoDb's -db.collection.find()
//mongodb = db.tasks.find({})
Task.find({}) //mognosee
.then(result => res.send(result))
.catch(err => res.send(err));

});

const Sample = mongoose.model("samples",sampleSchema);

app.post('/sample',(req,res) => {

let newSample = new Sample({
	name: req.body.name,
	isActive: req.body.isActive

});

	newSample.save((error,savedSample)=>{
	if(error){
		res.send(error);
	}else{
		res.send(savedSample);
	}

});

});

/*
	Create a new Get request method route to get All sample documents
	Send the array of sample documents in our postman client
	Else, catch the error and send the error in the client.


*/

app.get('/sample',(req,res) => {

Sample.find((error,savedSample)=>{
	if(error){
		res.send(error);
	}else{
		res.send(savedSample);
	}

})

});


//Schema
// const manggagamitSchema = new mongoose.Schema({
// 	username:String,
// 	isAdmin:Boolean

// });


// //
// const Manggagamit = mongoose.model("manggagamit",manggagamitSchema);

// app.post('/manggaGamit',(req,res) => {

// let newManggamit = new Manggagamit({
// 	username: req.body.username,
// 	isAdmin: req.body.isAdmin

// });

// 	newManggamit.save((savedManggagamit,error)=>{
// 	if(error){
// 		res.send(error);
// 	}else{
// 		res.send(savedManggagamit);
// 	}

// });

// });
//

//ACTIVITY

const userSchema = new mongoose.Schema({

username: String,
password: String

});

const Users = mongoose.model("user",userSchema);

app.post('/users',(req,res) => {

let newUsers = new Users({
	username: req.body.username,
	password: req.body.password

});

// 	newUsers.save((error,savedSample)=>{
// 	if(error){
// 		res.send(error);
// 	}else{
// 		res.send(savedSample);
// 	}

// });
newUsers.save()
.then(result => res.send({message: "Users Creation Successful"}))
.catch(error => res.send({message: "Error Users Creating"}));



});

app.get('/users',(req,res) => {

 Users.find({}) //mognosee
.then(result => res.send(result))
.catch(err => res.send(err));

// Users.find((error,savedUsers)=>{
// 	if(error){
// 		res.send(error);
// 	}else{
// 		res.send(savedUsers);
// 	}

// })

});


//[SECTION4] Entry Point Response
//bind the connection to the designated port
app.listen(port, () => console.log(`Server running at port ${port}`));